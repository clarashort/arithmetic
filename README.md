# README.md

This repository contains the source code described in C. S. Short and E. E. Swartzlander, Jr., "Performance evaluation of a sum error detection scheme for decimal arithmetic," to be presented at *Asilomar* 2019, preprint [available](https://cshort.io/assets/pdf/asilomar2019.pdf).

## Dependencies

In `script/common.tcl`, update `$synopsys_dir` to point to your tools (2015 or later):
- VCS
- Design Compiler
- IC Compiler
- PrimeTime
- DesignWare Foundation
- SAED 32/28nm PDK

## Usage

`[myemail=user@example.com] script/make.sh width [width ...]` -- synthesize, place, and route the model at the given bit widths, create a uniquely named tarball with the results and area/power/timing reports, and send an email to $myemail (if set) on completion

`script/sim.sh testname [param=value ...]` -- simulate testbench/tb_testname.v with the given parameters and write the output to a uniquely named log file

`cd <run-id> && ../script/process_results.py` -- print LaTeX code for the area and delay figures

`script/summarize_area.py <run-id>/i<impl>w<width>/eco_reports/area.rpt.csv` -- print a hierarchical breakdown of the total design area