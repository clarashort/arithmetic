########## set up build environment ##########
source "[file dirname [info script]]/common.tcl"
exec rm -rf ${eco_reports_dir}
open_mw_cel post_route
source -e -v ${constraints_file}

########## apply ECO fixes ##########
source ${eco_icc_script}
derive_pg_connection -all
legalize_placement -eco -incremental
route_zrt_eco \
    -utilize_dangling_wires true \
    -reroute modified_nets_first_then_others \
    -open_net_driven true \
    -reuse_existing_global_route true
verify_zrt_route
extract_rc

########## write results ##########
save_mw_cel -as post_eco
exec mkdir -p ${eco_reports_dir}
cd ${eco_reports_dir}
write_verilog "netlist.v"

########## write reports ##########
report_design -physical > "design.rpt"
report_timing -capacitance -transition_time \
    -input_pins -nets -delay max -nosplit > "timing_max.rpt"
report_timing -capacitance -transition_time \
    -input_pins -nets -delay min -nosplit > "timing_min.rpt"
report_placement_utilization -verbose > "placement_utilization.rpt"
report_qor > "qor.rpt"
report_constraints > "constraints.rpt"
report_area -hierarchy -nosplit > "area.rpt"
report_area -designware -nosplit > "area_dware.rpt"
report_area -physical -nosplit > "area_phys.rpt"
analyze_fp_rail -nets {VDD VSS} \
    -power_budget 15 \
    -voltage_supply 1.10 \
    > "ir_drop.rpt"

close_mw_cel
close_mw_lib
exit
