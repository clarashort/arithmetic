########## operating conditions ##########
set_operating_conditions \
    -max ${corners(slow)} \
    -min ${corners(fast)}

########## timing and DRC constraints ##########
set_load 1.5 [all_outputs]
set_max_delay 0.5 -from [all_inputs] -to [all_outputs]
set_input_transition 0.02 [all_inputs]
set_max_fanout 16 [current_design]

########## clocks and switching activity ##########
create_clock -name vclk -period 2.0
set_propagated_clock [all_clocks]
set_switching_activity \
    -type inputs \
    -static_probability 0.5 \
    -toggle_rate 0.25 \
    -base_clock vclk
