########## set up build environment ##########
source "[file dirname [info script]]/common.tcl"
exec rm -rf ${eco_pt_script} ${eco_icc_script}
read_milkyway -library ${mw_design_library} post_route
source -e -v ${constraints_file}

########## PrimeTime options ##########
set report_default_significant_digits 3
set timing_save_pin_arrival_and_slack true
set timing_all_clock_propagated true
set timing_save_pin_arrival_and_slack true
set timing_report_unconstrained_paths true
set timing_enable_multiple_clocks_par_reg true
set timing_remove_clock_reconvergence_pessimism true

########## ECO fix options ##########
set min_slack 0.02
set all_buffers {\
    NBUFFX4_HVT  AOBUFX2_HVT  NBUFFX8_HVT NBUFFX2_HVT NBUFFX16_HVT \
    AOBUFX4_HVT  AOBUFX1_HVT NBUFFX32_HVT AOBUFX4_RVT AOBUFX1_RVT \
    NBUFFX32_RVT NBUFFX4_RVT  AOBUFX2_RVT NBUFFX8_RVT NBUFFX2_RVT \
    NBUFFX16_RVT DELL*}


########## determine ECO fixes ##########
report_timing
fix_eco_drc -type max_fanout -verbose -buffer_list ${all_buffers}
fix_eco_timing -type setup -methods {size_cell} \
    -slack_lesser_than ${min_slack} -setup_margin ${min_slack} \
    -verbose

########## write results ##########
write_changes -format ptsh -output ${eco_pt_script}
write_changes -format icctcl -output ${eco_icc_script}

exit
