########## set up build environment ##########
source "[file dirname [info script]]/common.tcl"
exec rm -rf ${icc_reports_dir} ${icc_floorplan}
open_mw_cel post_synthesis
source -e -v ${constraints_file}
source ${Antenna_file}

######### floorplan parameters ##########
set aspect_ratio 1.0
set utilization 0.7
# power grid
set power_layers {M9 M8}
set skip_rings false
set rings {VDD VSS}
set ring_width 3.0
set ring_space 1.0
set ring_offset 1.0
set min_strap_width 0.0
set max_strap_width 1.0
set min_strap_count 1
# IR drop
set power_budget 30.0
set voltage_supply 1.05
set pad_current 20.0
# clock tree
set clock_layers {M4 M5 M6 M7}
set clock_max_transition 0.05
set clock_target_skew 0.05

########## floorplan ##########

if {${skip_rings}} {
    set io2core 0
} {
    set io2core [expr 2*(${ring_width} + ${ring_space}) + ${ring_offset}]
}
create_floorplan \
    -control_type aspect_ratio \
    -core_aspect_ratio ${aspect_ratio} \
    -core_utilization ${utilization} \
    -left_io2core ${io2core} \
    -right_io2core ${io2core} \
    -bottom_io2core ${io2core} \
    -top_io2core ${io2core}

########## pad plan (none) ##########

########## power grid ##########
    
set hlayer [lindex ${power_layers} 0]
set vlayer [lindex ${power_layers} 1]

# create PG nets
create_net -power VDD
create_net -ground VSS
derive_pg_connection -all

# specify constraints
#set_fp_rail_constraints -set_global -optimize_tracks 
set_fp_rail_constraints -add_layer -layer ${hlayer} -direction horizontal \
    -min_width ${min_strap_width} -max_width ${max_strap_width} \
    -min_strap ${min_strap_count} -spacing interleaving
set_fp_rail_constraints -add_layer -layer ${vlayer} -direction vertical \
    -min_width ${min_strap_width} -max_width ${max_strap_width} \
    -min_strap ${min_strap_count} -spacing interleaving
set_fp_rail_strategy -use_tluplus true
set_fp_power_pad_constraints -target_pad_current ${pad_current}

if {${skip_rings}} {
    set_fp_rail_constraints -skip_ring -extend_strap boundary
} {
    set_fp_rail_constraints -set_ring -nets ${rings} \
        -horizontal_ring_layer ${hlayer} -vertical_ring_layer ${vlayer} \
        -ring_width ${ring_width} -ring_spacing ${ring_space} \
        -ring_offset ${ring_offset}
    set bbox [get_attribute [get_die_area] bbox]
    lassign [join ${bbox}] xmin ymin xmax ymax
    set pad_offset [expr ${ring_offset} + 0.5*${ring_width}]
    foreach ring ${rings} {
        set x0 [expr ${xmin} + ${pad_offset}]
        set y0 [expr ${ymin} + ${pad_offset}]
        set x1 [expr ${xmax} - ${pad_offset}]
        set y1 [expr ${ymax} - ${pad_offset}]
        foreach x [list ${x0} ${x1}] {
            foreach y [list ${y0} ${y1}] {
                create_fp_virtual_pad \
                    -net ${ring} \
                    -layer ${hlayer} \
                    -point [list ${x} ${y}]
            }
        }
        set pad_offset [expr ${pad_offset} + ${ring_width} + ${ring_space}]
    }
}

synthesize_fp_rail \
    -nets {VSS VDD} \
    -voltage_supply ${voltage_supply} \
    -power_budget ${power_budget} \
    -synthesize_power_plan \
    -synthesize_power_pads
commit_fp_rail

########## clock tree ##########
set_clock_tree_options \
    -max_transition ${clock_max_transition} \
    -target_skew ${clock_target_skew} \
    -buffer_sizing TRUE \
    -buffer_relocation TRUE \
    -gate_sizing TRUE \
    -gate_relocation TRUE \
    -layer_list ${clock_layers}

########## initial placement ##########
check_physical_design -stage pre_place_opt
create_placement -quick 
legalize_placement -effort low -incremental -timing
check_physical_design -stage post_initial_placement

########## power/ground routing ##########
derive_pg_connection -all -reconnect
preroute_standard_cells

########## detailed placement ##########
set_buffer_opt_strategy -effort high
place_opt -effort high -area_recovery -congestion -power
psynopt -area_recovery -congestion -power
save_mw_cel -as post_place

########## detailed routing ##########
check_physical_design -stage pre_route_opt
route_opt -incremental -effort high -area_recovery -power -wire_size

########## verification ##########
verify_zrt_route
verify_lvs -ignore_floating_port
verify_pg_nets
extract_rc

########## write results ##########
save_mw_cel -as post_route
write_floorplan -all ${icc_floorplan}
exec mkdir -p ${icc_reports_dir}
cd ${icc_reports_dir}
write_verilog "netlist.v"

########## reports ##########
report_design -physical > "design.rpt"
report_timing -capacitance -transition_time \
    -input_pins -nets -delay max -nosplit > "timing_max.rpt"
report_timing -capacitance -transition_time \
    -input_pins -nets -delay min -nosplit > "timing_min.rpt"
report_placement_utilization -verbose > "placement_utilization.rpt"
report_qor > "qor.rpt"
report_constraints > "constraints.rpt"
report_area > "area.rpt"
analyze_fp_rail -nets {VDD VSS} \
    -power_budget ${power_budget} \
    -voltage_supply ${voltage_supply} \
    > "ir_drop.rpt"

close_mw_cel
close_mw_lib
exit
