set design_name "top"
set_host_options -max_cores 6

########## project hierarchy ##########
set root_dir [pwd]
set build_dir "${root_dir}/build"
set script_dir "${root_dir}/script"
set src_dir "${root_dir}/rtl"
cd ${build_dir}

set constraints_file "${script_dir}/constraints.tcl"
set mw_design_library "${build_dir}/${design_name}.mw"

# Design Compiler
set dc_reports_dir "${build_dir}/dc_reports"
set dc_work_dir "${build_dir}/WORK"
set dc_svf_file "${build_dir}/${design_name}.svf"

# IC Compiler
set icc_reports_dir "${build_dir}/icc_reports"
set icc_floorplan "${build_dir}/${design_name}.fp.tcl"

# PrimeTime (ECO)
set eco_pt_script "${build_dir}/eco_pt.tcl"
set eco_icc_script "${build_dir}/eco_icc.tcl"

# IC Compiler (ECO)
set eco_reports_dir "${build_dir}/eco_reports"

# PrimeTime PX
set ptpx_reports_dir "${build_dir}/ptpx_reports"

########## library hierarchy ##########
set synopsys_dir "/usr/local/packages/synopsys_2015"
set dw_lib_dir "${synopsys_dir}/syn/dw/dw_foundation/lib"
set pdk_dir "${synopsys_dir}/SAED32_EDK"
set lib_dir "${pdk_dir}/lib"
set tech_dir "${pdk_dir}/tech"

# standard cell library
array set corners {"slow" "ss0p95vn40c" "typ" "tt1p05v25c" "fast" "ff1p16v125c"}
array set stdcell_libs {"slow" "" "typ" "" "fast" ""}
set vth {"hvt" "rvt" "lvt"}
set nldm_reference_library [list]
set mw_reference_library [list]
foreach vt ${vth} {
    set stdcell_path "${lib_dir}/stdcell_${vt}"
    append nldm_reference_library "${stdcell_path}/db_nldm "
    append mw_reference_library "${stdcell_path}/milkyway/saed32nm_${vt}_1p9m "
    foreach {speed corner} [array get corners] {
        append stdcell_libs(${speed}) "saed32${vt}_${corner}.db "
        append stdcell_libs(${speed}) "saed32${vt}_pg_${corner}.db "
    }
}
unset stdcell_path

# tech library
set tech_mw_dir     "${tech_dir}/milkyway"
set Tech_file       "${tech_mw_dir}/saed32nm_1p9m_mw.tf"
set Map_file        "${tech_mw_dir}/saed32nm_tf_itf_tluplus.map"
set Antenna_file    "${tech_mw_dir}/saed32nm_ant_1p9m.tcl"
set Alf_file        "${tech_mw_dir}/saed32nm_em_1p9m.alf"
set Gds_map_file    "${tech_mw_dir}/saed32nm_1p9m_gdsout_mw.map"
set tech_rc_dir     "${tech_dir}/star_rcxt"
set Tlup_max_file   "${tech_rc_dir}/saed32nm_1p9m_Cmax.tluplus"
set Tlup_min_file   "${tech_rc_dir}/saed32nm_1p9m_Cmin.tluplus"
set Tlup_typ_file   "${tech_rc_dir}/saed32nm_1p9m_nominal.tluplus"
set Nxtgrd_max_file "${tech_rc_dir}/saed32nm_1p9m_Cmax.nxtgrd"
set Nxtgrd_min_file "${tech_rc_dir}/saed32nm_1p9m_Cmin.nxtgrd"
set Nxtgrd_typ_file "${tech_rc_dir}/saed32nm_1p9m_nominal.nxtgrd"

########## library setup ##########

if ![file exists ${mw_design_library}] {
    create_mw_lib ${mw_design_library} \
        -technology ${Tech_file} \
        -mw_reference_library ${mw_reference_library}
    open_mw_lib ${mw_design_library}
}

if {${synopsys_program_name} != "pt_shell"} {
    set_app_var synthetic_library "dw_foundation.sldb"
    set_tlu_plus_files \
        -max_tluplus ${Tlup_max_file} \
        -min_tluplus ${Tlup_min_file} \
        -tech2itf_map ${Map_file}
} {
    set synthetic_library ""
}

set_app_var search_path [concat ${search_path} ${nldm_reference_library}]
set_app_var target_library ${stdcell_libs(slow)}
set_app_var link_library [concat "*" ${target_library} ${synthetic_library}]

foreach vt ${vth} {
    set_min_library  "saed32${vt}_${corners(slow)}.db" \
        -min_version "saed32${vt}_${corners(fast)}.db"
    set_min_library  "saed32${vt}_pg_${corners(slow)}.db" \
        -min_version "saed32${vt}_pg_${corners(fast)}.db"
}
