#!/usr/bin/env ipython3 -i
from collections import namedtuple
import numpy as np
import pandas as pd
import re
import matplotlib.pyplot as plt
#import sys

impl_names = [
    'BCD',
    'BCD (DMR)',
    'BCD (LRC)',
    'Ripple',
    'LF (Depth)',
    'LF (Size)',
    'DW (Operator)',
    'DW (pparch)',
    'DW (apparch)',
]
impls = range(len(impl_names))
#widths = [int(w) for w in sys.argv if w.isnumeric()]
widths = [32, 64, 128, 256, 512]
useful_impls = [0,1,2,4,7]

testcases = pd.MultiIndex.from_product(
    [impls, widths],
    names=['impl', 'width']
)

Lookup = namedtuple('Lookup', ['file', 'pattern'])

post_synth_metrics = {
    'delay': Lookup('dc_reports/qor.rpt', '^  Critical Path Length: *([0-9.]*)$'),
    'power': Lookup('dc_reports/power.rpt', '^top *([0-9.e+-]* *){3} *([0-9.e+-]*) *100.0$'),
    'area': Lookup('dc_reports/area.rpt', '^Total cell area: *([0-9.]*)$'),
}

pre_eco_metrics = {
    'delay': Lookup('icc_reports/timing_max.rpt', '^  data arrival time *([0-9.]*) *$'),
    'area': Lookup('icc_reports/area.rpt', '^Total cell area: *([0-9.]*)$'),
}

post_route_metrics = {
    'delay': Lookup('ptpx_reports/timing.rpt', '^  data arrival time *([0-9.]*)$'),
    'power': Lookup('ptpx_reports/power.rpt', '^Total Power *= *([0-9.e+-]*) *\(100.00%\)$'),
    'area': Lookup('eco_reports/area.rpt', '^Total cell area: *([0-9.]*)$'),
}

def lookup(metrics):
    df = pd.DataFrame(index=testcases, columns=metrics.keys(), dtype=np.float64)
    for (name, (file, pattern)) in metrics.items():
        regex = re.compile(pattern)
        for (impl, width) in testcases:
            filename = f'i{impl}w{width}/{file}'
            with open(filename) as fh:
                for line in fh:
                    value = regex.match(line)
                    if value:
                        df.loc[(impl,width),name] = float(value.groups()[-1])
    return df

df0 = lookup(post_synth_metrics)
df1 = lookup(post_route_metrics)
df2 = lookup(pre_eco_metrics)

df0.loc[:,'power'] /= 1000 # uW to mW
df1.loc[:,'power'] *= 1000 # W to mW
for df in df0, df1, df2:
    df.loc[:,'area'] /= 1000 # um2 to um2*1000

def stats(df):
    return pd.DataFrame([df.min(), df.mean(), df.max(), df.std()], index=['min', 'mean', 'max', 'stdev'])

def plot_results(df, metric, **kwargs):
    df.loc[:, metric].unstack().T.plot.bar(**kwargs)
    #plt.xscale('log')
    plt.ylabel(metric)
    plt.ylim(0,2.5)
    names = [impl_names[i] for i in set(df.index.get_level_values('impl'))]
    plt.legend(names)

def latex_tables(df):
    for m in ['delay', 'area']:
        print(f'{m.capitalize()}', *widths, '\\\\', sep=' & ')
        for i in useful_impls:
            data = map(lambda x: round(x,2), df.loc[i,m])
            print(impl_names[i], *data, '\\\\', sep=' & ')
        print()

def latex_plots(df):
    for m in ['delay', 'area']:
        print(f'\\nextgroupplot[title={m.capitalize()}]')
        for i in useful_impls:
            data = map(lambda x: (x[0],round(x[1],2)), df.loc[i,m].items())
            print(
                f'\\addplot[{impl_names[i].lower()}] coordinates',
                '{', *data, '};'
            )
        print()

def relplot(df):
    for m in ['delay', 'area']:
        plot_results(df/df.loc[0,], m)
    plt.show()

synth_accuracy = (df0 - df1) / df1
eco_effect = (df1 - df2) / df2
redundancy_cost = (df1.loc[1:2,] - df1.loc[0,]) / df1.loc[0,]

#lf_vs_dw = df1.loc[4:5,] / df1.loc[6,:]

print()
print('Accuracy of post-synthesis results:', stats(synth_accuracy), sep='\n', end='\n\n')
print('Relative cost of ECO fixes:', stats(eco_effect), sep='\n', end='\n\n')
print('Relative cost of redundancy:', redundancy_cost, sep='\n', end='\n\n')
print('Actual post-route results', df1, sep='\n', end='\n\n')

df = df1.loc[useful_impls,]

latex_tables(df)
latex_plots(df/df.loc[0,])

# plot_some(plt.subplot(2,2,1), df2, 0, 2, 'delay', legend=False)
# plot_some(plt.subplot(2,2,2), df1, 0, 2, 'delay', legend=False)
# plot_some(plt.subplot(2,2,3), df2, 0, 2, 'area')
# plot_some(plt.subplot(2,2,4), df1, 0, 2, 'area')
# plt.show()
