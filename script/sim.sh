#!/bin/sh

top_dir="$(pwd)"
script_dir="${top_dir}/script"
build_dir="$(mktemp -d)"
src_dir="${top_dir}/rtl"
tb_dir="${top_dir}/testbench"
vcs_cmd="vcs -full64 -file ${script_dir}/vcs.cfg"

top_module="$1"
shift
params="${@/#/-pvalue+tb_${top_module}.}"
tb_file="${tb_dir}/tb_${top_module}.v"

cd ${build_dir}
${vcs_cmd} -y ${src_dir} ${tb_file} ${params}
cd ${top_dir}
mv "${build_dir}/vcs.log" vcs.$(basename ${build_dir}).log
rm -rf ${build_dir}
