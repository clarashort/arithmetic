#!/usr/bin/env ipython3 -i
import numpy as np
import pandas as pd
import sys

cells = [
    'top',
    'adder',
    'check.checker',
    'check.checker/bin',
    'check.checker/bin/complement\[[0-9]+\]',
    'check.checker/bin/complement\[[0-9]+\]/plus6',
    'check.checker/bin/digit_adder\[[0-9]+\]',
    'check.checker/bin/digit_adder\[[0-9]+\]/add_x_1',
    'check.checker/bin/digit_correct\[[0-9]+\]',
    'check.checker/bin/digit_correct\[[0-9]+\]/plus1',
    'check.checker/bin/digit_correct\[[0-9]+\]/plus6',
    'check.checker/bin/impl.carry',
    'check.checker/csa',
    'check.checker/sum_invert\[[0-9]+\]',
    'check.checker/sum_invert\[[0-9]+\]/carry4',
    'check.checker/sum_invert\[[0-9]+\]/minus6',
    'check.checker/sum_invert\[[0-9]+\]/plus6',
]

df = pd.read_csv(sys.argv[-1])
df.index = df['cell']
del df['cell']

print('global', 'local', 'cells', sep='\t')
for c in cells:
    regex = f'^{c}$'
    match = df.filter(regex=regex, axis=0)
    area_g = sum(match['global_area'])/1000
    area_l = sum(match['local_area'])/1000
    print(f'{area_g:6.3f}\t{area_l:6.3f}\t{c}')
