#!/bin/sh

wtxt="parameter width = "
itxt="parameter impl = "

log () {
    echo $(date): $1 | tee -a build/build.log
}

build () {
    impl=$1
    width=$2
    testcase=i${impl}w${width}

    sed -i -e "s/${itxt}[0-9]*;/${itxt}${impl};/" \
           -e "s/${wtxt}[0-9]*;/${wtxt}${width};/" rtl/top.v
    
    mkdir -p build alib-52 $testcase
    ln -s ../alib-52 build/

    log "start $testcase"
    log "start dc"
    dc_shell  -f    script/dc.tcl   2>&1 > build/dc.log
    log "start icc"
    icc_shell -f    script/icc.tcl  2>&1 > build/icc.log
    log "start pt"
    pt_shell  -file script/pt.tcl   2>&1 > build/pt.log
    log "start eco"
    icc_shell -f    script/eco.tcl  2>&1 > build/eco.log
    log "start ptpx"
    pt_shell  -file script/ptpx.tcl 2>&1 > build/ptpx.log
    log "finish $testcase"

    mv build/*{.log,_reports} $testcase
    rm -rf build
    rm *.{log,txt}
}

if which dc_shell && which icc_shell && which pt_shell; then
    echo "All modules loaded."
else
    echo "Run 'module load syn syn/icc syn/primetime' first."
    exit 1
fi

if [ "$*" = "" ]; then
    echo "Usage: $0 width [width ...]"
    exit 1
fi

run_id=arith.$(git rev-parse --abbrev-ref HEAD).$(hostname -s).$$
wdir=/tmp/$run_id
echo "Running in $wdir ..."
mkdir $wdir
cp -R script/ $wdir/
cp -R rtl/ $wdir/
pushd $wdir

for w in "$@"; do
    cp rtl/top.v.bcd rtl/top.v
    for i in {0..2}; do
        build $i $w
    done
    cp rtl/top.v.no_bcd rtl/top.v
    for i in {3..8}; do
        build $i $w
    done
    rm rtl/top.v
done

rm -rf alib-52
popd
tar cjf ${run_id}.tar.bz2 -C /tmp $run_id && rm -rf $wdir
 
if [ "$myemail" != "" ]; then
    echo $run_id | mail -s "job $run_id finished" "$myemail"
fi