########## set up build environment ##########
source "[file dirname [info script]]/common.tcl"
exec rm -rf ${ptpx_reports_dir}
read_milkyway -library ${mw_design_library} post_eco
set power_enable_analysis true
source -e -v ${constraints_file}

########## timing analysis options ##########
set report_default_significant_digits 3
set timing_save_pin_arrival_and_slack true
set timing_all_clock_propagated true
set timing_save_pin_arrival_and_slack true
set timing_report_unconstrained_paths true
set timing_enable_multiple_clocks_par_reg true
set timing_remove_clock_reconvergence_pessimism true

########## power analysis options ##########
set power_default_toggle_rate 0.25
set power_default_static_probability 0.5
set power_reset_negative_internal_power true
set power_reset_negative_extrapolation_value true
set_power_analysis_options -cells [get_cells -hierarchical]

########## timing reports ##########
exec mkdir -p ${ptpx_reports_dir}
cd ${ptpx_reports_dir}
report_timing -nosplit > "timing.rpt"
report_constraints > "constraints.rpt"
report_constraint -all_violators > "all_violators.rpt"
report_analysis_coverage -status_details {untested} > "coverage.rpt"
report_disable_timing > "disabled_checks.rpt"
report_bottleneck -nosplit > "bottleneck.rpt"

########## power reports ##########
check_power
update_power
report_power -nosplit > "power.rpt"
report_power -nosplit -verbose -cell_power -hierarchy > "power_cell.rpt"
report_power -nosplit -verbose -leaf -hierarchy > "power_leaf.rpt"
report_power -nosplit -verbose -net_power > "power_net.rpt"

exit
