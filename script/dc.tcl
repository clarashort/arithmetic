########## set up build environment ##########
source "[file dirname [info script]]/common.tcl"
exec rm -rf "${dc_reports_dir}" "${dc_work_dir}" "${dc_svf_file}"

########## elaborate ##########
set_svf ${dc_svf_file}
define_design_lib dw_foundation -path ${dw_lib_dir}
define_design_lib WORK -path ${dc_work_dir}
set search_path [concat ${search_path} ${src_dir}]
analyze -f verilog [glob -tails -directory ${src_dir} "*.v"]
elaborate -library WORK ${design_name}
link
uniquify
remove_unconnected_ports -blast_buses [find -hierarchy cell "*"]
check_design -nosplit

########## create power and ground nets ##########
create_supply_net VDD
create_supply_net VSS
create_supply_set sset_top -function {power VDD} -function {ground VSS}
create_power_domain pd_top -supply {primary sset_top} -include_scope
create_supply_port VDD_IN -domain pd_top
create_supply_port VSS_IN -domain pd_top
connect_supply_net VDD -ports VDD_IN
connect_supply_net VSS -ports VSS_IN

# these voltages must match slow and fast corner operating voltages
set_voltage 0.95 -min 1.16 -object_list {VDD}
set_voltage 0.00 -min 0.00 -object_list {VSS}

########## application settings ##########
set_app_var auto_wire_load_selection true
set_app_var enable_recovery_removal_arcs true
set_app_var report_default_significant_digits 3
set_app_var synlib_enable_analyze_dw_power 1
set_app_var timing_enable_multiple_clocks_per_reg true
set_app_var verilogout_no_tri true
set_app_var verilogout_show_unconnected_pins true
set_dp_smartgen_options -all_options auto -optimize_for speed
set_dynamic_optimization true
set_fix_multiple_port_nets -all
set_host_options -max_cores 6
set_leakage_optimization true
set_power_prediction true
set_verification_priority -all

########## apply constraints ##########
source ${constraints_file}
if [file exists ${icc_floorplan}] {
    read_floorplan ${icc_floorplan}
}

########## compile ##########
compile_ultra -retime
optimize_netlist -area

########## write results ##########
write_milkyway -output "post_synthesis"
exec mkdir -p "${dc_reports_dir}"
cd "${dc_reports_dir}"
write_file -format verilog -hierarchy -output "netlist.v"

########## write reports ##########
check_design > "check_design.rpt"
check_timing > "check_timing.rpt"
report_qor > "qor.rpt"
report_threshold_voltage_group > "threshold_voltage_group.rpt"
report_area -hierarchy -nosplit -physical > "area.rpt"
report_power -hierarchy -nosplit > "power.rpt"
report_constraint -nosplit -all_violators > "constraint.rpt"
report_resources > "resources.rpt"

close_mw_lib
exit
