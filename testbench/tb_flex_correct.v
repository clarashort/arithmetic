module tb_flex_correct;
    reg [3:0] Y;
    reg G, P, C, DEC;
    wire [3:0] Yp6, S, Sref;
    integer errors;

    flex_digit_correct dut (Y, G, P, C, DEC, S);
    assign Sref = Y + C + 4'd6*(DEC & (G | (P & C)));

    initial begin
        $timeformat(-9,0,"",1);
        {Y, C, G, P, DEC} = 0;
        errors = 0;
        repeat (160) begin
            #1
            if (S !== Sref) begin
                $display(
                    "Y=%h C=%b G=%b P=%b DEC=%b -> S=%h (expect %h)",
                    Y, C, G, P, DEC, S, Sref);
                errors = errors + 1;
            end // if
            {Y, C, G, P, DEC} = {Y, C, G, P, DEC} + 1;
        end // repeat
        $display("%0d testcases, %0d errors.", $time, errors);
        $finish;
    end // initial
endmodule // tb_plus6
