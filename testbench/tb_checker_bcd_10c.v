module tb_checker_bcd_10c;
    parameter width = 16;
    localparam [width-1:0] max_num = 10**(width/4);
    reg [width-1 : 0] binA, binB, errs;
    reg SUB, CLK;
    wire [width-1 : 0] binS, decA, decB, decS;
    wire ERR;
    integer pass, fail, ii;

    function automatic [width-1:0] bin2dec (input [width-1:0] bin);
        reg [2*width-1:0] str;
        integer result;
        begin
            $sformat(str, "%d", bin);
            result = $sscanf(str, "%h", bin2dec);
        end
    endfunction // bin2dec

    function automatic [width-1:0] dec2bin (input [width-1:0] dec);
        reg [2*width-1:0] str;
        integer result;
        begin
            $sformat(str, "%h", dec);
            result = $sscanf(str, "%d", dec2bin);
        end
    endfunction // dec2bin

    checker_mixed_2c_10c #(width) dut (decA, decB, SUB, 1'b1, decS, ERR);

    assign binS = SUB ? ((max_num + binA) - binB) % (max_num) : binA + binB;
    assign decA = bin2dec(binA);
    assign decB = bin2dec(binB);
    assign decS = bin2dec(binS) ^ errs;

    always #0.5 CLK <= ~CLK;

    initial begin
        $timeformat(-9, 0, "", 1);
        CLK=0; binA=0; binB=0; SUB=0; errs=0; pass=0; fail=0;
    end

    always @(posedge CLK) begin
        if (ERR === |errs)
            pass = pass + 1;
        else begin
            fail = fail + 1;
            $display("t=%t A=%h B=%h S=%h^%h=%h err=%b",
                $time, decA, decB, bin2dec(binS), errs, decS, ERR);
        end

        // binA = $random % max_num;
        // binB = $random % max_num;

        errs = ($random % 2) ? $random : 0;

        {binB, SUB} = {binB, SUB} + 1;
        if (binB > max_num) begin
            binB = 0;
            binA = binA + 1;
            if (!(binA % 100)) $display("%d", binA);
        end
        if (binA > max_num) begin
            if (pass) $display("%6d tests passed", pass);
            if (fail) $display("%6d tests failed", fail);
            $display("%6d tests completed.", pass + fail);
            $finish;
        end
    end
endmodule // tb
