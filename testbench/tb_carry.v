module tb_carry;
    parameter log_width = 2;
    localparam width = 1<<log_width;
    reg [width-1:0] G, P;
    wire [width-1:0] C0, C1, C2;
    integer pass, fail1, fail2, fail;

    carry_ripple #(width) ripple (C0, , G, P);
    carry_ladner_fischer #(0, log_width) mindepth (C1, , G, P);
    carry_ladner_fischer #(log_width, log_width) minsize (C2, , G, P);

    task blanks (input integer num);
        for (num = num; num > 0; num = num - 1)
            $write(" ");
    endtask // blanks

    initial begin
        $timeformat(-9, 0, "", 1);
        {G, P} = 0;
        pass = 0;
        fail1 = 0;
        fail2 = 0;
        fail = 0;
        forever #1 begin
            case ({C2 !== C0, C1 !== C0})
                2'b00: begin
                    pass = pass + 1;
                end
                2'b01: begin
                    $write("t=%t G=%b P=%b C=%b", $time, G, P, C0);
                    fail1 = fail1 + 1;
                    $display(" C1^C=%b", C1 ^ C0);
                end
                2'b10: begin
                    $write("t=%t G=%b P=%b C=%b", $time, G, P, C0);
                    fail2 = fail2 + 1;
                    blanks(width + 6);
                    $display(" C2^C=%b", C2 ^ C0);
                end
                2'b11: begin
                    $write("t=%t G=%b P=%b C=%b", $time, G, P, C0);
                    fail = fail + 1;
                    $display(" C1^C=%b C2^C=%b", C1 ^ C0, C2 ^ C0);
                end
            endcase
            {G, P} = {G, P} + 1;
            if (!{G, P}) begin
                if (pass)  $display("%5d tests: both duts passed", pass);
                if (fail1) $display("%5d tests: only dut1 failed", fail1);
                if (fail2) $display("%5d tests: only dut2 failed", fail2);
                if (fail)  $display("%5d tests: both duts failed", fail);
                $finish;
            end
        end
    end

endmodule // tb
