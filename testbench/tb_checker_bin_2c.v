module tb_checker_bin_2c;
    parameter width = 16;
    reg [width-1 : 0] A, B, errs;
    reg SUB, CLK;
    wire [width-1 : 0] S;
    wire ERR;
    integer pass, fail;

    checker_mixed_2c_10c #(width) dut (A, B, SUB, 1'b0, S, ERR);

    assign S = (SUB ? A - B : A + B) ^ errs;

    always #0.5 CLK <= ~CLK;

    initial begin
        $timeformat(-9, 0, "", 1);
        CLK=0; A=0; B=0; SUB=0; errs=0; pass=0; fail=0;
    end

    always @(posedge CLK) begin
        if (ERR === |errs)
            pass = pass + 1;
        else begin
            fail = fail + 1;
            $display("t=%t A=%h B=%h SUB=%b SUM=%h E=%h S=%h err=%b",
                $time, A, B, SUB, S ^ errs, errs, S, ERR);
        end
        
        // binA = $random % max_num;
        // binB = $random % max_num;
        
        errs = ($random % 2) ? $random : 0;
        
        {B, SUB} = {B, SUB} + 1;
        if ({B, SUB} == 0) begin
            A = A + 1;
            if (A == 0) begin
                if (pass) $display("%6d tests passed", pass);
                if (fail) $display("%6d tests failed", fail);
                $display("%6d tests completed.", pass + fail);
                $finish;
            end
            if (!(A % 100)) $display("%d", A);
        end
    end
endmodule // tb
