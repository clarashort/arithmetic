module tb_plus1;
    reg [3:0] A;
    reg EN;
    wire [3:0] Y, Yref;
    integer errors;

    plus1 dut (A, EN, Y);
    assign Yref = EN ? A + 4'h1 : A;

    initial begin
        $timeformat(-9,0,"",1);
        {A, EN} <= 0;
        errors = 0;
        repeat (32) begin
            #1
            if (Y !== Yref) begin
                $display(
                    "%h + %h = %h (expect %h)",
                    A, EN ? 4'h1 : 4'h0, Y, Yref
                );
                errors = errors + 1;
            end // if
            {A, EN} <= {A, EN} + 1;
        end // repeat
        $display("%0d testcases, %0d errors.", $time, errors);
        //$finish;
    end // initial
endmodule // tb_plus6
