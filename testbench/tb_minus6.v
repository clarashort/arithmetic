module tb_minus6;
    reg [3:0] A;
    reg EN;
    wire [3:0] Y, Yref;
    integer errors;

    minus6 dut (A, EN, Y);
    assign Yref = EN ? A - 4'd6 : A;

    initial begin
        $timeformat(-9,0,"",1);
        {A, EN} <= 0;
        errors = 0;
        repeat (20) begin
            #1
            if (Y !== Yref) begin
                $display(
                    "%d + %d = %d (expect %d)",
                    A, EN ? 4'd6 : 4'd0, Y, Yref
                );
                errors = errors + 1;
            end // if
            {A, EN} <= {A, EN} + 1;
        end // repeat
        $display("%0d testcases, %0d errors.", $time, errors);
        //$finish;
    end // initial
endmodule // tb_plus6
