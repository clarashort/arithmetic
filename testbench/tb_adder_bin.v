module tb_adder_bin;
    parameter digits = 2;
    localparam width = digits*4;
    reg [width-1:0] A, B;
    reg SUB, DEC;
    wire [width-1:0] SUM, Sref;
    integer errors;

    adder_flex #(width) dut (A, B, SUB, DEC, SUM);
    assign Sref = SUB ? A - B : A + B;

    initial begin
        $timeformat(-9, 0, "", 1);
        {A, B, SUB, DEC} = 0;
        errors = 0;
        repeat (2<<(width*2)) begin
            #1 if (SUM !== Sref) begin
                errors = errors + 1;
                $display("%t: %h %c %h = %h (expect %h)",
                    $time, A, SUB ? 45 : 43, B, SUM, Sref);
            end // if
            {A, B, SUB} <= {A, B, SUB} + 1'b1;
        end // repeat
        $display("%t testcases, %d failures", $time, errors);
    end // initial
endmodule // tb_adder_bin
