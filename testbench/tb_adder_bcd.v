module tb_adder_flex_bcd;
    parameter width = 8;
    parameter [width-1:0] max_num = 10**(width/4);
    integer pass, fail, ii;

    function automatic [width-1:0] bin2dec (input [width-1:0] bin);
        reg [2*width-1:0] str;
        integer result;
        begin
            $sformat(str, "%d", bin);
            result = $sscanf(str, "%h", bin2dec);
        end
    endfunction // bin2dec

    function automatic [width-1:0] dec2bin (input [width-1:0] dec);
        reg [2*width-1:0] str;
        integer result;
        begin
            $sformat(str, "%h", dec);
            result = $sscanf(str, "%d", dec2bin);
        end
    endfunction // dec2bin

    reg [width-1 : 0] binA, binB;
    reg SUB, CLK;
    wire [width-1 : 0] decA, decB, decS, decRef, binS, binRef;
    
    assign decA = bin2dec(binA);
    assign decB = bin2dec(binB);
    assign binS = dec2bin(decS);
    assign decRef = bin2dec(binRef);

    adder_flex #(width) dut (decA, decB, SUB, 1'b1, decS);
    assign binRef = SUB ? ((max_num + binA) - binB) %(max_num) : binA + binB;
    always #0.5 CLK <= ~CLK;

    initial begin
        $timeformat(-9, 0, "", 1);
        CLK=0; binA=0; binB=0; SUB=0; pass=0; fail=0;
    end

    always @(posedge CLK) begin
        if (decS === decRef) begin
            pass = pass + 1;
        end
        else begin
            fail = fail + 1;
            if (fail < 1000) begin
                $write("t=%t: %h%c%h=%h (expect %h): ",
                    $time, decA, SUB ? "-" : "+", decB, decS, decRef);
                $display("%d%c%d=%d (expect %d)",
                    binA, SUB ? "-" : "+", binB, binS, binRef);
                //$display("g=%b p=%b i=%b o=%b c=%b ",
                //    dut.g, dut.p, dut.carry.in, dut.carry.out, dut.c);
            end
        end
        {binB, SUB} = {binB, SUB} + 1;
        if (binB >= max_num) begin
            binB = 0;
            binA = binA + 1;
            if (!(binA % 100)) $display("%d", binA);
        end
        if (binA >= max_num) begin
            if (pass) $display("%6d tests passed", pass);
            if (fail) $display("%6d tests failed", fail);
            $display("%6d tests completed.", pass + fail);
            $finish;
        end
    end
endmodule // tb
