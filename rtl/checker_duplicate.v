module checker_duplicate (A, B, SUB, DEC, SUM, ERR);
    parameter width = 64;
    parameter fast = 1;
    input [width-1 : 0] A, B, SUM;
    input SUB, DEC;
    output ERR;
    
    wire [width-1 : 0] Sref;

    adder_flex #(width, fast) bin (A, B, SUB, DEC, Sref);
    assign ERR = |(SUM ^ Sref);
endmodule // checker_duplicate
