module gp_cell (g, p, g1, p1, g2, p2);
    // The carry-propagate function for binary addition.
    output g, p;
    input  g1, p1, g2, p2;
    wire   c;

    and and_p (p, p1, p2);
    and and_c (c, p1, g2);
    or  or_g  (g, g1, c);
endmodule // gp_cell
