module plus1 (A, EN, Y);
    input [3:0] A;
    input EN;
    output [3:0] Y;
   
    assign Y[3] = A[3] ^ (EN & A[2] & A[1] & A[0]);
    assign Y[2] = A[2] ^ (EN & A[1] & A[0]);
    assign Y[1] = A[1] ^ (EN & A[0]);
    assign Y[0] = A[0] ^ EN;
endmodule // plus1
