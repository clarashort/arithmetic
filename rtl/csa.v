module csa (A, B, C, SV, CV);
    parameter integer width = 64;
    input  wire [width-1:0] A, B, C;
    output wire [width-1:0] SV, CV;

    assign SV = A ^ B ^ C;
    assign CV = (A & B) | (B & C) | (A & C);
endmodule // csa
