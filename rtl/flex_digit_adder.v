module flex_digit_adder (A, B, DEC, Y, G, P);
    input [3:0] A, B;
    input DEC;
    output [3:0] Y;
    output G, P;
    wire carry;

    assign {carry, Y} = A + B;
    assign G = carry | (DEC & Y[3] & (Y[2] | Y[1]));
    assign P = Y[3] & (Y[2] ^ DEC) & (Y[1] ^ DEC) & Y[0];
endmodule // flex_digit_adder
