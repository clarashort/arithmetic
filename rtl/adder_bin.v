module adder_bin (A, B, SUB, S);
    parameter width = 64;
    parameter fast = 1;
    parameter k = 0;
    input [width-1 : 0] A, B;
    input SUB;
    output [width-1 : 0] S;

    wire [width-1:0] bb, g, p, c;
    
    assign bb = SUB ? ~B : B;
    assign g = A & bb;
    assign p = A ^ bb;
    assign S = p ^ c;

    generate
        case (fast)
            0: begin: impl
                carry_ripple #(width) carry (
                    c, , {g[width-2:0], SUB}, {p[width-2:0], 1'b0}
                );
            end // impl
            1: begin: impl
                carry_ladner_fischer #(k, $clog2(width)) carry (
                    c, ,{g[width-2:0], SUB}, {p[width-2:0], 1'b0}
                );
            end // impl
        endcase // fast
    endgenerate
endmodule // adder_bin
