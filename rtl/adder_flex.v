module adder_flex (A, B, SUB, DEC, S);
    parameter width = 64;
    parameter fast = 0;
    input [width-1 : 0] A, B;
    input SUB, DEC;
    output [width-1 : 0] S;

    localparam ndigits = width / 4;
    wire [width-1 : 0] bb, y;
    wire [ndigits-1 : 0] g, p, c;

    flex_complement    complement    [ndigits-1:0] (B, SUB, DEC, bb);
    flex_digit_adder   digit_adder   [ndigits-1:0] (A, bb, DEC, y, g, p);
    flex_digit_correct digit_correct [ndigits-1:0] (y, g, p, c, DEC, S);
    
    generate
        case (fast)
            0: begin: impl
                carry_ripple #(ndigits) carry (
                    c, , {g[ndigits-2:0], SUB}, {p[ndigits-2:0], 1'b0}
                );
            end // impl
            1: begin: impl
                carry_ladner_fischer #(0, $clog2(ndigits)) carry (
                    c, , {g[ndigits-2:0], SUB}, {p[ndigits-2:0], 1'b0}
                );
            end // impl
        endcase // fast
    endgenerate
endmodule // adder_flex
