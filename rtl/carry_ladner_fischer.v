module carry_ladner_fischer (go, po, gi, pi);
    // Depth is minimized when k=0;
    // size  is minimized when k=logn.
    parameter integer k = 0;
    parameter integer logn = 6;
    localparam  n = 1 << logn;
    output [n-1 : 0] go, po;
    input  [n-1 : 0] gi, pi;
    genvar ii;

    generate
        if (logn == 0) begin: onewire // Figure 5 of [1]
            buf buf_go (go[0], gi[0]);
            buf buf_po (po[0], pi[0]);
        end // onewire
/*
        else if (logn == 1) begin: onecell // Figure 5 of [1]
            buf buf_go (go[0], gi[0]);
            buf buf_po (po[0], pi[0]);
            gp_cell gp (go[1], po[1], gi[1], pi[1], gi[0], pi[0]);
        end // onecell

        else if (logn == 2) begin: twocell // Figure 5 of [1]
            wire [1:0] gmid, pmid;
            buf buf_go0 (go[0], gi[0]);
            buf buf_po0 (po[0], pi[0]);
            gp_cell gp_in1 (gmid[1], pmid[1], gi[3], pi[3], gi[2], pi[2]);
            gp_cell gp_in0 (gmid[0], pmid[0], gi[1], pi[1], gi[0], pi[0]);
            gp_cell gp_out1 (go[3], po[3], gmid[1], pmid[1], gmid[0], pmid[0]);
            gp_cell gp_out0 (go[2], po[2], gi[2], pi[2], gmid[0], pmid[0]);
            buf buf_go1 (go[1], gmid[0]);
            buf buf_po1 (po[1], pmid[0]);
        end // twocell
*/
        else if (k == 0) begin: min_depth // Figure 3 of [1]
            wire [n-1 : 0] gmid, pmid;

            carry_ladner_fischer #(1, logn-1) recurse_low (
                gmid[0 +: n/2], pmid[0 +: n/2],
                gi  [0 +: n/2], pi  [0 +: n/2]
            );
            carry_ladner_fischer #(0, logn-1) recurse_high (
                gmid[n/2 +: n/2], pmid[n/2 +: n/2],
                gi  [n/2 +: n/2], pi  [n/2 +: n/2]
            );
            gp_cell gp_out [n-1 : n/2] (
                go  [n/2 +: n/2], po  [n/2 +: n/2],
                gmid[n/2 +: n/2], pmid[n/2 +: n/2],
                gmid[n/2-1],      pmid[n/2-1]
            );
            buf buf_go [n/2-1 : 0] (
                go  [0   +: n/2],
                gmid[0   +: n/2]
            );
            buf buf_po [n/2-1 : 0] (
                po  [0   +: n/2],
                pmid[0   +: n/2]
            );
        end // min_depth

        else begin: min_size // Figure 4 of [1]
            wire [n/2-1 : 0] gmid_in, pmid_in, gmid, pmid;

            buf buf_gl (go[0], gi[0]);
            buf buf_pl (po[0], pi[0]);
            buf buf_gh (go[n-1], gmid[n/2-1]);
            buf buf_ph (po[n-1], pmid[n/2-1]);
                
            carry_ladner_fischer #(k-1, logn-1) recurse (
                gmid, pmid,
                gmid_in, pmid_in
            );

            for (ii = 0; ii < n/2; ii = ii + 1) begin: in_layer
                gp_cell gp_in (
                    gmid_in[ii], pmid_in[ii],
                    gi [2*ii+1], pi [2*ii+1],
                    gi [2*ii  ], pi [2*ii  ]
                );
            end // in_layer

            for (ii = 1; ii < n/2; ii = ii + 1) begin: out_layer
                buf buf_go (
                    go  [2*ii-1],
                    gmid[  ii-1]
                );
                buf buf_po (
                    po  [2*ii-1],
                    pmid[  ii-1]
                );
                gp_cell gp_out (
                    go  [2*ii], po  [2*ii],
                    gi  [2*ii], pi  [2*ii],
                    gmid[ii-1], pmid[ii-1]
                );
            end // out_layer
        end // min_size
    endgenerate
endmodule // carry_ladner_fischer
