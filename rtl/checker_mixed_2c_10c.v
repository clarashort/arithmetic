module checker_mixed_2c_10c #(
    parameter integer width = 64
) (
    input [width-1 : 0] X, Y,
    input SUB, DEC,
    input [width-1 : 0] SUM,
    output ERR
);
    // Figure 5: Detection of 10's-complement add/sub errors
    
    wire [width-1 : 0] yb, sb, sv, cv, hv;
    // wire [width/4-1 : 0] invalid_digits;

    // digitwise decimal correction
    mixed_sum_invert sum_invert [width/4-1 : 0] (
        X, Y, SUB, DEC, SUM, yb, sb/*, invalid_digits*/);

    // equation (8): conventional carry-save adder
    csa #(width) csa (X, yb, sb, sv, cv);
    assign hv[0] = SUB;
    assign hv[width-1 : 1] = cv[width-2 : 0];

    // equation (9): error checker
    assign ERR = ~&(sv ^ hv) /*| (|invalid_digits)*/;

endmodule // checker_bcd_10c
