module flex_complement (A, EN, DEC, Y);
    input [3:0] A;
    input EN, DEC;
    output [3:0] Y;

    wire [3:0] Ap;
    plus6 plus6 (A, DEC, Ap);
    assign Y = EN ? ~Ap : A;
endmodule // bcd_9s_complement
