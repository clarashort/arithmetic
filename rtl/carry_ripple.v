module carry_ripple (go, po, gi, pi);
    parameter integer width = 64;
    output [width-1:0] go, po;
    input  [width-1:0] gi, pi;
    wire   [width-1:0] g, p;
    genvar ii;

    gp_cell gp [width-1:0] (
        g, p,
        gi, pi,
        {g[width-2:0], gi[0]}, {p[width-2:0], pi[0]}
    );

    buf buf_go [width-1:0] (go, g);
    buf buf_po [width-1:0] (po, p);
endmodule // carry_ripple
