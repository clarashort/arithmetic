module carry4 (A, B, G, P);
    input [3:0] A, B;
    output G, P;
    wire [3:0] gi, pi;
    wire [1:0] gmid, pmid;
    
    assign gi = A & B;
    assign pi = A ^ B;

    assign P = &pi;
    assign G = gi[3] |
               pi[3] & gi[2] |
               pi[3] & pi[2] & gi[1] |
               pi[3] & pi[2] & pi[1] & gi[0];
endmodule // carry4
