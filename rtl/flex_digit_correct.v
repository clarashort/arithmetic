module flex_digit_correct (Y, G, P, C, DEC, S);
    input [3:0] Y;
    input G, P, C, DEC;
    output [3:0] S;

    wire [3:0] Yp;
    plus6 plus6(Y, DEC & (G | P & C), Yp);
    plus1 plus1 (Yp, C, S);
endmodule // flex_digit_correct
