module mixed_sum_invert (X, Y, SUB, DEC, SUM, YB, SB);
    // Figures 5(b)-5(c) and 6: Decimal correction and sum inversion
    input [3:0] X, Y, SUM;
    input SUB, DEC;
    output [3:0] YB, SB;

    wire [3:0] Yp, ya;
    wire g, p, suminv;

    plus6 plus6 (Y, DEC, Yp);
    assign ya = SUB ? ~Y : Yp;
    carry4 carry4 (X, ya, g, p);
    minus6 minus6 (ya, DEC & ~g & ~p, YB);
    assign suminv = DEC & p;

    assign SB[3] = ~SUM[3];
    assign SB[2] = ~SUM[2] ^ (SUM[3] & suminv);
    assign SB[1] = ~SUM[1] ^ (SUM[3] & suminv);
    assign SB[0] = ~SUM[0];

endmodule // mixed_sum_invert
