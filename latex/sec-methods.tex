%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

All code described in this section is available under the MIT License at \url{https://arith.cshort.io/}.

\subsection{Implementation}

A parameterized Verilog model is developed for the ``mixed binary/\-BCD addition/\-subtraction error checker'' described above and in \cite[Sec. II-C]{VazAnt17} . The checker itself is fairly straightforward (less than 50 lines of code), but the adder being checked (and, by extension, the error checker's testbench) requires additional effort, as Verilog lacks native support for BCD arithmetic. The \texttt{\$sformat} and \texttt{\$sscanf} system tasks \cite[Clause 17.2]{Verilog01} can convert an integer to a hexadecimal string and read it back as a decimal value in testbench code, but are obviously not synthesizable. Instead, a ten's-complement binary/BCD adder similar to \cite{Agrawal74} is implemented, consisting of a digit-wise addition stage, a carry-propagation stage, and a final digit-correction stage.

This poses another implementation problem: while the first and last stages are simple (again, only about 50 lines of code), the available synthesis tools only infer efficient carry-lookahead circuits as part of their proprietary datapath IP, which lacks integer BCD arithmetic blocks. An additional Verilog model is implemented that generates synthesizable Ladner-Fischer trees is implemented and used in its minimum-depth configuration for both the binary and BCD adders. This model improves on previously available implementations such as \cite{Nag15} by supporting any power-of-two bit width, providing an adjustable tradeoff between gate count and logic depth, and having a simple recursive structure that corresponds directly to the original figures in \cite{LadFis80}.

\subsection{Validation}
\label{sec:methods-validation}

The adders are validated through exhaustive simulation at an input width of 16 bits (4 decimal digits), which is sufficient to exercise all the bitwise and digit-wise logic in the adder, as well as the base case and both recursive cases for the Ladner-Fischer tree generator. Validating both addition and subtraction this way requires $(10^{4})^2 \times 2 = 2 \times 10^8$ BCD test cases (or about 20 minutes of simulation time) and $(2^{16})^2 \times 2 \approx 9 \times 10^9$ binary test cases (or an overnight simulation run). The error checkers are validated during the same simulation runs; in each test case, the input to the error checker is the correct sum XORed with a random error value, which is zero (i.e. no error) in 50\% of the test cases and uniformly distributed in the other 50\%.

\subsection{Performance Evaluation}

The design is synthesized, placed, and routed using the Synopsys 32/28nm Educational Design Kit \cite{GolEtAl13} at widths from 32 to 512 bits. Mixed binary/BCD adders are synthesized with no error checking, with dual-modular redundancy, and with the proposed error checker. For comparison purposes, a non-redundant, minimum-depth Ladner-Fischer adder (without BCD support) is also synthesized, as well as a fast parallel-prefix adder from Synopsys's DesignWare library \cite{Syn15}.