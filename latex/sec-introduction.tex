%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As CMOS logic densities increase and transistor sizes decrease, online error detection is becoming more and more important for acceptable reliability. The long residue checker (LRC) circuit \cite{SulSwa12} detects all errors in a fixed-point adder with a simple carry-save addition between the operands and the one's complement of the result:
\begin{align}
    \notag SV &= X \oplus Y \oplus \overline{S} \\
    \notag CV &= X Y \vee (X \vee Y) \overline{S}
\end{align}
The carry term $2 \times CV$ is equal to the one's complement of the sum term $SV$ if and only if the adder output is correct:
\begin{align}
    \notag X + Y &= S \\
    \notag X + Y + \overline{S} &= S + \overline{S} \\
    \notag SV + 2 \times CV &= 111\cdots 1_2 \\
    \notag SV + 2 \times CV &= SV + \overline{SV} \\
    \label{eqn:unsigned-identity}2 \times CV &= \overline{SV}
\end{align}

The identity in (\ref{eqn:unsigned-identity}) holds for all values of $A$ and $B$, resulting in robust error detection, and can be checked using only bitwise operations (i.e. no carry propagation is required), reducing the area needed for error checking compared to a dual-modular redundant (DMR) adder.

\subsection{Two's-Complement Binary Error Checking}

To support two's-complement arithmetic, $B$ is conditionally complemented at the input to the carry-save adder, and a carry-in bit is appended to $CV$ at the output of the carry-save adder:
\begin{align}
    \notag Y^B &= Y \oplus {sub} \\
    \notag SV &= X \oplus Y^B \oplus \overline{S} \\
    \notag CV &= X Y^B \vee (X \vee Y^B) \overline{S}
\end{align}
For two's-complement subtraction, i.e. ${sub} = 1$:
\begin{align}
    \notag X - Y &= S \\
    \notag X - Y - S &= 0 \\
    \notag X + (\overline{Y} + 1) + (\overline{S} + 1) &= 0 \\
    \notag (X + Y^B + \overline{S}) + 1 &= -1 \\
    \notag (SV + 2 \times CV) + {sub} &= 111 \cdots 1_2\\
    \notag SV + 2 \times CV + {sub} &= SV + \overline{SV} \\
    \label{eqn:2c-identity}2 \times CV + {sub} &= \overline{SV}
\end{align}
For two's-complement addition, ${sub}=0$, and (\ref{eqn:2c-identity}) becomes equivalent to (\ref{eqn:unsigned-identity}).

\subsection{Ten's-Complement Decimal Error Checking}
\label{sec:intro-10c}

V\'azquez and Antelo \cite{VazAnt17} describe an LRC-based error checker modified to support ten's-complement binary-coded decimal (BCD) arithmetic. Let $F$ be the (pseudo)-BCD value whose binary representation is all ones, i.e. $F = \sum_{i} 15 \times 10^{i}$. Ten's-complement addition may be checked with:
\begin{align}
    \notag X + Y &= S \\
    \notag X + Y - S &= 0 \\
    \notag X + Y + (F - S) &= F \\
    X + Y + \overline{S} &= F
\end{align}
and continuing as in (\ref{eqn:unsigned-identity}). Subtraction may be checked with:
\begin{align}
    \notag X - Y &= S \\
    \notag X - Y - S &= 0 \\
    \notag X - Y + \overline{S} &= F \\
    \label{eqn:10c-identity} X + (Y^D + sub) + \overline{S} &= F
\end{align}
and continuing as in (\ref{eqn:2c-identity}), where $Y^D$ is the conditional nine's complement of $Y$:
\begin{equation}
    Y^D_i = 
    \begin{cases}
        Y_i, & {sub} = 0 \\
        \overline{Y_i + 6}, & {sub} = 1
    \end{cases}
\end{equation}
To calculate the left-hand side of (\ref{eqn:10c-identity}) without carry propagation, \cite{VazAnt17} describes three correction steps. First, the carry-in signal to each digit is eliminated by approximating the carry function
\begin{equation*}
    C_{i+1} = (X_i + Y^D_i + C_i \geq 10)
\end{equation*}
with the alive (i.e. carry-propagate) function
\begin{equation}
    A_i = (X_i + Y^D_i \geq 9) .
\end{equation}
Second, the carry-out signal from each digit is eliminated by adding 6 to each decimal digit where $A_i = 1$. Finally, the error introduced by substituting $A_i$ for $C_{i+1}$ is corrected by subtracting 6 from each decimal digit $\overline{S_i}$ where $A_i=1$ and $C_{i+1}=0$. Since the only possible values for $S_i$ when $A_i = 1$ are 9 (when $C_{i+1} = 0$) and 0 (when $C_{i+1} = 1$), this last correction is implemented as a simple conditional bitwise operation \cite[Fig. 5(c)]{VazAnt17}:
\begin{equation}
    \label{eqn:oops} S^B_i = \overline{S_i} \wedge
    \begin{cases}
        1001_2, & A_i = 1 \textrm{ and } S_{i,3} = 1 \\
        1111_2, & A_i = 0 \textrm{ or } S_{i,3} = 0
    \end{cases}
\end{equation}
Continuing from (\ref{eqn:10c-identity}), the carry-save addition becomes:
\begin{align}
    X + (Y^D + sub) + S^B &= F \\
    \notag SV + 2 \times CV + sub &= F \\
    \notag 2 \times CV + sub &= \overline{SV}
\end{align}
which is identical to (\ref{eqn:2c-identity}).

The proposed architecture is presented along with a set of favorable area and delay estimates. However, these estimates are hand-calculated and given in terms of FO4 inverter delays and NAND2 cell areas\footnote{One FO4 delay is the delay of a minimum-size inverter with a fan-out of four minimum-size inverters; one NAND2 area is the area of the standard cell that implements a minimum-size two-input NAND gate. The Synopsys 32/28nm educational library \cite{GolEtAl13} has a nominal FO4 delay of 0.03 ns and a NAND2 area of 2.54 $\mathrm{\mu m^2}$.} for a generic CMOS technology. No reference implementation is provided.